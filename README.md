# Team Graph Tactics

TFT graph visualization Tool

This tool isn’t endorsed by Riot Games and doesn’t reflect the views or opinions
of Riot Games or anyone officially involved in producing or managing League of
Legends.

# Getting Static Data
Running `./scripts/fetch_data.js` should download the required image and place
them under `web/img`.

If you wish to browse this data an archive can be downloaded by get the version
from here https://ddragon.leagueoflegends.com/api/versions.json then download
the file here https://ddragon.leagueoflegends.com/cdn/dragontail-${VERSION}.tgz

# Testing
Run `docker run --rm -it -p 8080:80 -v $(pwd):/usr/share/nginx/html:ro nginx`
then visit http://localhost:8080/web/index.html