#!/usr/bin/env node
/**
 * This script downloads the image data from Riot Games public API.
 */
const fs = require('fs');
const http = require('http');
const https = require('https');
const data = require('../src/data.json');

process.chdir(__dirname + "/..");



async function download(url, filePath) {
    const proto = !url.charAt(4).localeCompare('s') ? https : http;

    return new Promise((resolve, reject) => {
        const file = fs.createWriteStream(filePath);
        let fileInfo = null;

        const request = proto.get(url, response => {
            if (response.statusCode !== 200) {
                reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
                return;
            }

            fileInfo = {
                mime: response.headers['content-type'],
                size: parseInt(response.headers['content-length'], 10),
            };

            response.pipe(file);
        });

        // The destination stream is ended by the time it's called
        file.on('finish', () => resolve(fileInfo));

        request.on('error', err => {
            fs.unlink(filePath, () => reject(err));
        });

        file.on('error', err => {
            fs.unlink(filePath, () => reject(err));
        });

        request.end();
    });
}

// Delete the fetched data and remake the folders
if (fs.existsSync('web/img')){
    fs.rmdirSync('web/img', { recursive: true });
}
fs.mkdirSync('web/img/champion', { recursive: true });



const URL = 'ddragon.leagueoflegends.com';
data.champions.data.forEach( champion => {
    const name = champion[0].replace('-', '');
    const skin = champion[2];

    if(name === "silco") {
        fs.copyFileSync('scripts/silco_0.jpg', 'web/img/champion/silco_0.jpg');
        return;
    }

    let name_capitalized = name[0].toUpperCase() + name.substr(1);;
    if(name_capitalized === "Reksai") name_capitalized = "RekSai";
    if(name_capitalized === "Tahmkench") name_capitalized = "TahmKench";
    if(name_capitalized === "Missfortune") name_capitalized = "MissFortune";
    if(name_capitalized === "Jarvaniv") name_capitalized = "JarvanIV";

    const file_path = `web/img/champion/${name}_${skin}.jpg`;
    const full_url = `https://${URL}/cdn/img/champion/tiles/${name_capitalized}_${skin}.jpg`
    download(full_url, file_path);
});