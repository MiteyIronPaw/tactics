import cytoscape from 'cytoscape';
import coseBilkent from 'cytoscape-cose-bilkent';
import DATA from './data.json';
import CONFIG from './config.json';


function main() {

    const names = DATA.champions.data.map(champion => champion[0])

    const elements = {
        nodes: DATA.champions.data.map(
            champion => {
                return {
                    data: {
                        id: champion[0],
                        synergies: champion[3],
                        active: false
                    },
                    classes: "cost_" + champion[1]
                }
            }
        ),

        edges: []
    };



    function map_edges(champions_list) {
        const output = [];

        names.forEach(name => {
            const champion_1 = champions_list.filter(
                champion => champion[0] === name
            )[0];
            champion_1[3].forEach(synergy => {

                const synergy_champions = champions_list.filter(
                    champion => champion[3].includes(synergy)
                );

                synergy_champions.forEach(champion_2 => {
                    if (champion_1 !== champion_2) {
                        output.push({
                            group: 'edges', 
                            data: {
                                source: champion_1[0],
                                target: champion_2[0]
                            }
                        })
                    }
                })
            })
        })

        return output;
    }

    const edges = map_edges(DATA.champions.data);

    const STYLE = cytoscape.stylesheet().selector('node').css({
        // A regular hexagon height width ratio is 1.1547005
        'height': 80,
        'width': 92.4,
        'background-fit': 'cover',
        'border-width': 5,
        'shape': 'hexagon',
    }).selector('.cost_1').css({
        'border-color': '#858585',
    }).selector('.cost_2').css({
        'border-color': '#156831',
    }).selector('.cost_3').css({
        'border-color': '#12407c',
    }).selector('.cost_4').css({
        'border-color': '#893088',
    }).selector('.cost_5').css({
        'border-color': '#b89d27',
    });

    const href = window.location.href;
    const dir = href.substring(0, href.lastIndexOf('/')) + "/";

    DATA.champions.data.forEach(champion => {
        let url = dir + DATA.champions.path
        url += champion[0].replace(/-/g, "") + "_" + champion[2]
        url += DATA.champions.extension
        STYLE.selector('#' + champion[0]).css({ 'background-image': url })
    })

    const layout_config = {
        name: 'cose-bilkent',
        padding: 10,
        // 'randomize: false' seems to reduce how much the graph moves when the 
        // edges change
        randomize: false,
    }

    cytoscape.use(coseBilkent);

    const cy = cytoscape({
        container: document.getElementById('cy'),
        boxSelectionEnabled: false,
        autounselectify: true,
        style: STYLE,
        elements: elements,
        layout: layout_config,
        ...CONFIG
    });


    cy.on('tap', function(event) {
        if(event.target._private.group === "nodes") {
            const champion = event.target._private.data;
    
            // If it's active remove the edges
            if(champion.active) {
                cy.remove('edge[source=\'' + champion.id + '\']');
                champion.active = false;
    
            // If it's inactive add the edges
            } else {
                const siblings = edges.filter(
                    edge => edge.data.source === champion.id
                );
        
                cy.add(siblings)
                champion.active = true;
            }
    
            cy.layout(layout_config).run();
        }
    });
}



main();
